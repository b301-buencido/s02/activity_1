package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String[] args) {

        int isLeapYear;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a year to be checked if a leap year: ");
        isLeapYear = scanner.nextInt();


        if (isLeapYear % 4 == 0) {

            if (isLeapYear % 100 != 0 || isLeapYear % 400 == 0 ){

                System.out.println(isLeapYear + " is a leap year");
            }
        }

        else {

            System.out.println(isLeapYear + " is NOT a leap year");
        }
    }
}
