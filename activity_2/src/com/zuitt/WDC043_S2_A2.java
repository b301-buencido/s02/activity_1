package com.zuitt;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {

    public static void main (String[] args) {

        int[] primeNumbers = new int[5];


        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println(Arrays.toString(primeNumbers));



        System.out.println("---------------------------------");

        ArrayList<String> friends = new ArrayList<String>();

        friends.add(0, "John");
        friends.add(1, "Jane");
        friends.add(2, "Chloe");
        friends.add(3, "Zoey");

        System.out.println("My friends are: " + friends);


        System.out.println("---------------------------------");

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
